/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : toutiao

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-03-08 17:33:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for funny
-- ----------------------------
DROP TABLE IF EXISTS `funny`;
CREATE TABLE `funny` (
  `funny_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_genre` varchar(255) DEFAULT NULL,
  `behot_time` datetime DEFAULT NULL,
  `chinese_tag` varchar(255) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `has_gallery` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `is_feed_ad` varchar(255) DEFAULT NULL,
  `media_avatar_url` varchar(255) DEFAULT NULL,
  `media_url` varchar(255) DEFAULT NULL,
  `middle_mode` varchar(255) DEFAULT NULL,
  `more_mode` varchar(255) DEFAULT NULL,
  `single_mode` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `source_url` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `tag_url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `comments_count` varchar(255) DEFAULT NULL,
  `document` longtext,
  PRIMARY KEY (`funny_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
